# You can run `time  zsh -i -c exit` to measure startup in total.
# Uncomment the following line + zprof at the end to profile startupt calls.
# See: https://stevenvanbael.com/profiling-zsh-startup
# zmodload zsh/zprof

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# export HIST_STAMPS='%d/%m/%y %T'

# Path to your oh-my-zsh installation.
export ZSH=${HOME}/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# TO use agnoster, https://medium.freecodecamp.org/jazz-up-your-zsh-terminal-in-seven-steps-a-visual-guide-e81a8fd59a38 run git clone https://github.com/powerline/fonts.git && cd fonts && ./install.sh
# ZSH_THEME="agnoster"

# Uncomment the following line to enable command auto-correction.
 ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  # git
  zsh-autosuggestions
)


# Add some stuff to PATH
# export PATH=$HOME/homebrew/bin/:$PATH  # Dis path is faulty. Dont install homebrew at $HOME.
pathadd() {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH"}:$1"
    fi
}
# pathadd "/usr/local/Homebrew/bin/"
# pathadd "$HOME/homebrew/opt/coreutils/libexec/gnubin"
pathadd "/sbin"
pathadd "/usr/sbin"
pathadd "$HOME/.local/bin/"
pathadd "$HOME/.cargo/bin/"
pathadd "${HOME}/go/bin"

export PYENV_ROOT="$HOME/.pyenv"
pathadd "$PYENV_ROOT/bin"
# TODO: Figure out which of the following commands are needed and when. They are
# a bit slow, so try to avoid running them every time:
pathadd "$PYENV_ROOT/shims"
# NOTE: pyenv is pretty slow… that's why I only use init -. Probably I'll figure
# out to init virtualenv as well when needed in a good way.
# eval "$(pyenv init -)"
# eval "$(pyenv virtualenv-init -)"

####### History stuff #######
# Why in gods name does my history file disappear?!: https://unix.stackexchange.com/a/575102
# export HIST_STAMPS='%d/%m/%y %T'
export HISTSIZE=999999999
export HISTFILE="$HOME/.zsh_history"
export SAVEHIST=$HISTSIZE
setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ":start:elapsed;command" format.

# To save every command before it is executed (this is different from bash's history -a solution):
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.

# To retrieve the history file everytime history is called upon.
setopt SHARE_HISTORY             # Share history between all sessions.

# Get nice timestamp format on 'history' command (do it before sourcing oh-my-zsh)
export HIST_STAMPS='%d/%m/%y %T'

# Let's override the TERM var when using vanilla kitty. It will otherwise be a
# problem during ssh for many hosts as they don't recognize the terminal
# `xterm-kitty`
if [[ $TERM =~ kitty ]]; then
    TERM=xterm-256color
fi

# Autojump stuff. Pretty nice.
source /usr/share/autojump/autojump.zsh

# This order is good...
if setopt | grep nounset &>/dev/null; then
    reset_u='set -u'
fi
set +u
source $ZSH/oh-my-zsh.sh
eval $reset_u
unset reset_u
test -f $HOME/.env_public.sh && source $HOME/.env_public.sh || true
test -f $HOME/.env_private.sh && source $HOME/.env_private.sh || true
test -f $HOME/.env_work.sh && source $HOME/.env_work.sh || true

# Turn off autocorrect
unsetopt correct_all

# fzf magic
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_DEFAULT_COMMAND="find -L"
# zprof

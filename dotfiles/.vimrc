"TODO:
    "- https://github.com/amix/vimrc
    "- Clean up vimrc: group things together, make them collapsable, remove junk and duplicates
    "- Set up proper gotodefinition for more languages (python, ansible,…),
    "  probably via som lsp kinda thing such as ale.


call plug#begin()
" FileManagement:
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
" prova https://github.com/kien/ctrlp.vim för buffers, jmfr me fzf?
"Plug 'ctrlpvim/ctrlp.vim' "sök i mru filer, nuvarande directory, ctags etc.
Plug 'junegunn/fzf'  " Fuzzy find files
Plug 'mileszs/ack.vim'   " Grep files' contents
" Good ack guide https://beyondgrep.com/documentation/
Plug 'tpope/vim-fugitive' "git integration till vim. se vilken commit rader ändrats etc

" Autocompletion:
"Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'ervandew/supertab' " superawesome tab-completion

" Text:
Plug 'preservim/vim-textobj-quote'
Plug 'kana/vim-textobj-user'  "Needed by 'preservim/vim-textobj-quote'
Plug 'chrisbra/unicode.vim'  " Nice and ez way to find unicode chars `UnicodeSearch! …` is great!

" Misc:
Plug 'Raimondi/delimitMate' "automatiskt lägg till stängmotsvarighet till '({ etc. bekvämt.
Plug 'scrooloose/nerdcommenter' "enkla kommentarer för flera lines. mkt fint
Plug 'tpope/vim-surround' "lägg till/ändra tecken som ska omringa chars/words/meningar/etc.
Plug 'tpope/vim-repeat' "repetera grejer. bla repetera surround kommandon.
Plug 'christoomey/vim-tmux-navigator' "supernajs för att röra sig mellan terminalfönster från ett vim fokuserat fönster
Plug 'dahu/vim-fanfingtastic' "Riktigt trevligt plugin för multiline f/t/F/T. Ska fungera tillsammans med Tim Popes repeat
Plug 'tpope/vim-dispatch' " async Dispatch of commands, make etc. very najs
Plug 'bronson/vim-trailing-whitespace' "röd text för trailing whitespace. verkar najs. är det async?
Plug 'vitalk/vim-simple-todo' "Najs för att arbeta me markdown listor (- [ ], osv).
"Plug 'Yggdroot/indentLine' "visa indent level, tabs etc... https://vi.stackexchange.com/questions/422/displaying-tabs-as-characters
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}  "Opens a webbrowser to view the markdown

" Nice language support
" Python:
Plug 'vim-python/python-syntax' "python syntax highlighting. looks allright
Plug 'Vimjas/vim-python-pep8-indent' "Very nice to have proper indentation levels in python :)
Plug 'nvie/vim-flake8' "Pep8 and pyflakes
" Golang:
" Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
" TODO: Only make it fixlints manually?
Plug 'govim/govim'
" Misc:
" Note that ale is sometimes super slow. Possibly just enable/disable on a few
" filetypes etc.
Plug 'dense-analysis/ale'

" Colorschemes:
Plug 'morhetz/gruvbox'
" Plug 'arcticicestudio/nord-vim'
" Plug 'altercation/vim-colors-solarized'

" Powerline:
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

"exuberant-ctags
"neomake, good for pytest ? https://coderoncode.com/tools/2017/04/16/vim-the-perfect-ide.html

" Quarantine Zone:
" …

"" Generic LSP plugins for all languages
" TODO: Wth with ale?
Plug 'prabirshrestha/vim-lsp'
" Plug 'mattn/vim-lsp-settings'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'rhysd/vim-lsp-ale'

" Seems to include a lot of packages, such as python-syntax etc.
" Plug 'sheerun/vim-polyglot'

" Freezer:
" Plug 'preservim/vim-markdown'  " Rätt schysst, men … allt för långsamt…

call plug#end()


" TODO: something else for markdown or
" TODO: Add shfmt as shell lint as well?
let g:ale_linters = {
      \ 'sh':         ['shellcheck', 'language_server'],
      \ 'zsh':         ['shellcheck', 'language_server'],
      \ 'yaml':       ['yamllint'],
      \ 'markdown':   ['mdl'],
      \ 'dockerfile': ['hadolint'],
      \ 'ruby':       ['rubocop', 'ruby'],
      \ 'json':       ['jq'],
      \ }


let g:ale_completion_enabled = 1  " TODO: Fix dis
let g:ale_fix_on_save = 0
let b:ale_sh_shfmt_options='-i 4 -s -ci -ln bash'
" let g:ale_fixers.json = ['jq']
" let g:ale_fixer.sh = ['shfmt']
" TODO: Make it work for different files
" let b:ale_fixers=[ 'shfmt' , 'black' ]


let g:ale_linters_explicit = 1
" let g:ale_yaml_yamlfix_options = 'YAMLFIX_SEQUENCE_STYLE="keep_style"'
" let g:ale_yaml_yamlfix_options = 'sequence_style = keep_style'

" HINT: You can run fixers manually, like e.g. `ALEFix black`
let g:ale_fixers = {
      \ 'sh':         ['shfmt'],
      \ 'zsh':         ['shfmt'],
      \ 'json':       ['jq'],
      \ 'python':     ['black'],
      \ 'markdown':   ['pandoc'],
      \ 'yaml':       ['yamlfix'],
      \ }

" TODO: Add some kind of python lsp
" https://github.com/dense-analysis/ale/blob/master/ale_linters/python/pylint.vim
"     \ 'python': ['pyls'],

" let g:ale_python_pyls_executable = 'pylsp'
" let g:ale_completion_enabled = 1

" let g:ale_linters = {
"       'sh'=         {'shellcheck', 'language_server'},
"       'yaml'=       {'yamllint'},
"       'markdown'=   {'mdl'},
"       'dockerfile'= {'hadolint'},
"       'ruby'=  {'rubocop', 'ruby'},
"       }

" Set this. Airline will handle the rest.
" let g:airline#extensions#ale#enabled = 1
"

"""""""""""""
" FZF magic brother

" Search all buffers and also files. SUUUper niiiice :)
" FROM: https://github.com/junegunn/fzf.vim/issues/1401#issuecomment-1399352918
" command! FzfFilesAndBuffers call fzf#run({
" \ 'source':  reverse(s:all_files()),
" \ 'sink':    'tabedit',
" \ 'down':    '40%',
" \ })

let g:fzf_action = {
    \ 'ctrl-t': 'tab split',
    \ 'ctrl-x': 'split',
    \ 'ctrl-v': 'vsplit' }

" fzf#wrap in order to add ctrl-t -x -v etc
command! FzfFilesAndBuffers call fzf#run(fzf#wrap({
\ 'source':  reverse(s:all_files()),
\ }))

" \ 'ctrl-t': 'tab split',
" \ 'ctrl-x': 'split',
" \ 'ctrl-v': 'vsplit' }

function! s:all_files()
  return extend(
    \ reverse(
      \ filter(
        \ split(
          \ system('find . -type f -not -path "*/.git/*"'),
          \ "\n"
        \ ),
        \ '!empty(v:val)'
      \ ),
    \ ),
  \ map(filter(range(1, bufnr('$')), 'buflisted(v:val)'), 'bufname(v:val)'))
endfunction

" Search current and old buffers, pretty nice. IDK if it should be combined…
" FROM: https://github.com/junegunn/fzf/wiki/Examples-(vim)#filtered-voldfiles-and-open-buffers
" TODO: Do the wrap thingy
" TODO: Think about combining it with files FzfFilesAndBuffers (e.g. combine
" all_files and all_files2 … …. or … maybe we want this still? and combine it
" with FZFBuffers?! … idk…, it's ncie to be able to check all old buffers
" without checking all files under current dir (e.g. when running in home dir))?
command! FZFMru call fzf#run({
\ 'source':  reverse(s:all_files2()),
\ 'sink':    'edit',
\ 'options': '-m -x +s',
\ 'down':    '40%' })

function! s:all_files2()
  return extend(
  \ filter(copy(v:oldfiles),
  \        "v:val !~ 'fugitive:\\|NERD_tree\\|^/tmp/\\|.git/'"),
  \ map(filter(range(1, bufnr('$')), 'buflisted(v:val)'), 'bufname(v:val)'))
endfunction


" Select from buffers. IDK if i'll use it much…
" FROM: https://github.com/junegunn/fzf/wiki/Examples-(vim)#select-buffer
function! s:buflist()
  redir => ls
  silent ls
  redir END
  return split(ls, '\n')
endfunction

function! s:bufopen(e)
  execute 'buffer' matchstr(a:e, '^[ 0-9]*')
endfunction

" TODO: think about removing this perhaps…
command! FZFBuffers call fzf#run({
\   'source':  reverse(<sid>buflist()),
\   'sink':    function('<sid>bufopen'),
\   'options': '+m',
\   'down':    len(<sid>buflist()) + 2
\ })<CR>



"""""""""""""

augroup filetypes
  au!
  autocmd BufNewFile,BufRead Dockerfile* set filetype=dockerfile
augroup END

""""""""" STATUS LINE """""""""
" Always show the status line
set laststatus=0
" Format the status line:l:
"set statusline=%.40F%m%m%m%r%h\ %w\ %=\ Line:\ %l:%c%V

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Remember info about open buffers on close
" https://vi.stackexchange.com/questions/12777/do-not-reopen-buffers-of-last-session
set viminfo^=%


"airline/powerline
let g:airline_powerline_fonts = 1  " turns on arrows in status line (instead of rectangular ones)
" No idea what the following ones do… maybe check it out someday TODO?
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|' "'¥'

"arrows
" let g:Powerline_symbols = 'fancy'
" let g:airline_powerline_fonts = 1
" set guifont=Liberation\ Mono\ for\ Powerline\ 10


""""""""" COLORS, THEMES, ETC """""""""
"set t_Co=256
" set Vim-specific sequences for RGB colors
"let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
"let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
"set termguicolors

" theme
"väldigt coolt färgscheman nedan, men ser lite mysko ut i terminalvim :(
"colorscheme nord
"colorscheme gotham
"colorscheme mustang
"colorscheme solarized
"let g:solarized_termcolors=256
colorscheme gruvbox
set background=dark

"needed for tmux bg color match with vim https://unix.stackexchange.com/questions/197391/background-color-mismatch-in-vim-and-tmux
set term=screen-256color

" encoding
set encoding=utf-8
set fileencoding=utf-8  " When is this actually needed?
" set guifont=Monospace\ 13

" iskeyword sets the characters that should be considered a word. some
" colorschemes already set it though, so i should have it after that...
set iskeyword+=ö
set iskeyword+=ä
set iskeyword+=å


""""""""" KEY MAPS """""""""
imap jk <Esc>
" inoremap ii <esc>

" inoreabbrev ... <C-K>.,     "Not so good in comparison
inoremap ... <C-K>.,

function! TextUnicodeMappings()
    " TODO: make some easier mapping than :…:
    " Nice Unicode mappings etc:
    " `ctrl-K` inserts digraph. `ctrl-v U` inserts unicode char.
    " e.g `inoremap <buffer> :) <C-v>U1f642 ` maps to only local buffer instead
    " of globally, which is nice man (don't want it in general, only text files)
    " Insert 💪. Note that the `U` needs to be capitalized which doesn't seem to
    " match what people say online… idk what that's about:
    inoremap <buffer> :muscle: <C-v>U1f4aa 
    " 👍 :
    inoremap <buffer> :thumbsup: <C-v>U1f44d 
    " 🫡 :
    inoremap <buffer> :saluting_face: <C-v>U1fae1 
    " 😀  (grin):
    inoremap <buffer> :smiley: <C-v>U1f600 
    inoremap <buffer> :smile: <C-v>U1f600 
    inoremap <buffer> :D <C-v>U1f600 
    " slight smiling face 🙂 :
    inoremap <buffer> :) <C-v>U1f642 
    " 😞 :
    inoremap <buffer> :( <C-v>U1f61E 

    " ❤ :
    inoremap <buffer> :heart: <C-v>U2764 
    inoremap <buffer> <3 <C-v>U2764 
    " 🤣 :
    inoremap <buffer> :rofl: <C-v>U1F923 

    " 😂 tears of joy:
    inoremap <buffer> :laugh: <C-v>U1F602 
    inoremap <buffer> :haha: <C-v>U1F602 
    " 🤔 :
    inoremap <buffer> :thinking: <C-v>U1f914 
    "  😉:
    inoremap <buffer> :wink: <C-v>U1f609 
    inoremap <buffer> ;) <C-v>U1f609 
    " 🤩 :
    inoremap <buffer> *D <C-v>U1f929 
    " ಠ_ಠ:
    inoremap <buffer> <.< ಠ_ಠ
    " 👀 (EYES):
    inoremap <buffer> o.o <C-v>U1f440 
    "  🎉 (party popper / tada):
    inoremap <buffer> :tada: <C-v>U1f389 
    "  🥳 (party popper 2):
    inoremap <buffer> :party: <C-v>U1f973 
endfunction

" Neat curly quoting stuff (works with surround with `q` for double curly quote,
" and `Q` for single curly quotes).
set nocompatible
" Why use augroup: https://vi.stackexchange.com/questions/9455/why-should-i-use-augroup
augroup textobj_quote
    autocmd!
    autocmd FileType markdown call textobj#quote#init()
    " educate replaces " with “ during insertmode. HINT: :ToggleEducate to toggle!
    autocmd FileType text call textobj#quote#init({'educate': 0})

augroup END

autocmd FileType markdown call TextUnicodeMappings()
autocmd FileType text call TextUnicodeMappings()
" TODO: add vimrc here as well?

" Remap VIM 0 to first non-blank character
map 0 ^
"fix inconsistent Y
nnoremap Y y$

" Paste what is in the buffer, and don't let vim interpret shit like it want
nnoremap <F2> :set invpaste paste?<CR>
imap <F2> <C-O>:set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode


" Fast gunslinger reload
map <F5> :so $VIM_RC <ENTER>
map <F6> :e $VIM_RC<ENTER>
" map <F6> :vsplit $VIM_RC<ENTER>
map <F9> :ALEFix<ENTER>
cmap w!! w !sudo tee % > /dev/null

" Nerdtree
map <C-n> :NERDTreeToggle<CR>
let NERDTreeKeepTreeInNewTab=1
let NERDTreeShowHidden=1
"https://superuser.com/questions/387777/what-could-cause-strange-characters-in-vim
"let g:NERDTreeDirArrowExpandable = '>'
"let g:NERDTreeDirArrowCollapsible = '^'
"let g:NERDTreeDirArrows=0

" change standard copy/paste buffer,
" unnamed=`*` (highlight/select),
" unnamedplus=`+` (ctrl-c/v)
"set clipboard^=unnamed
"set clipboard^=unnamedplus


" who's the dear leader of custom mappings
let mapleader = " "
" TODO: figure out some nice and clever leader-namespacing?
" NOTEs on namespacing:
" - nerdcommenter has some in <leader>c…
"   https://github.com/preservim/nerdcommenter#default-mappings
" glorious and dear vim leader declarations
"<Bs> - Backspacke
"<ENTER> - Enter
"<ESC> - Escape

map <leader>buf :e ~/buffer<cr>
map <leader>cd :cd %:p:h
map <leader>e :UnicodeSearch! 
map <leader>f /
map <leader>g :Git blame<ENTER>
"map <leader>h <C-w>s
"map <leader>i <C-]> <ENTER>
map <leader>j :
" adds large letter to name
map <leader>na :call Name()<ENTER>
map <leader>no :nohl <ENTER>
" map <leader>o :b# <ENTER>
"map <leader>s <C-w>s
"map <leader>t :tnext<ENTER>
map <leader>lg :Dispatch lazygit<ENTER>
map <leader>l gu
map <leader>u gU
"map <leader>v <C-w>v
map <leader>yw viw"9y
map <leader>pw viw"9p

" Buffer magic
" Copy paste buffers * is the select register, and + is for copy paste
" Great links:
" https://stackoverflow.com/a/3997110
" https://vi.stackexchange.com/questions/84/how-can-i-copy-text-to-the-system-clipboard-from-vim)
"   `"*` is highlight buffer
"   `"+` is OS ctrl+c buffer
"   `""` and `"0` are vim copy-paste buffers
map <leader>y "+y
"map <leader>p :set paste "+p :set nopaste
map <leader>p :set paste <ESC>"+p:set nopaste <ESC>
map <leader>P :set paste <ESC>"+P:set nopaste <ESC>
map <C-p> :set paste <ESC>"*p:set nopaste <ESC>
map <C-P> :set paste <ESC>"*P:set nopaste <ESC>


" map <leader>0 viw"0p<ESC>
map <leader>0 "0p<ESC>

" Write and quit
map <leader>w :w<ENTER>
map <leader>x :x<ENTER>
map <leader>q :bd <ENTER>
map <leader>å :q <ENTER>
map <leader>ö :bd <ENTER>

" Programming short commands
map <leader>pdb oimport pdb; pdb.set_trace()<ESC>
"map <leader>try otry:<ENTER>except:<ENTER>import pdb; pdb.set_trace()<ESC>
map <leader>bb ggO#!/usr/bin/env bash<ESC>
map <leader>py ggO#!/usr/bin/env python3<ESC>
" map <leader>7 0g081lF r<ENTER><ESC>>>
" map <leader>8 0g081lF r<ENTER><ESC>
" map <leader>9 0g081lF r<ENTER><ESC>J
map <leader><leader> gql

map <leader>ag :Ack --smart-case 
" search with ack from visual selection https://stackoverflow.com/a/28011266 :
vnoremap <Leader>ag y:Ack <C-r>=fnameescape(@")<CR><CR>

" markdown related bindings
map <leader>mb ysiw`<ESC>
map <leader>mB ysiW`<ESC>
map <leader>mv :MarkdownPreview <ENTER>
" Code in-line (back-tick in-line)
map <leader>bi :let @r="``"<ENTER>"rp
map <leader>mbi 0ys$``
map <leader>mb$ ys$``
" Code Block (back-tick block)
map <leader>mbc :let @r="```\n```"<ENTER>"rp

" base64 encode/decode:
" https://stackoverflow.com/questions/7845671/how-to-execute-base64-decode-on-selected-text-in-vim#comment113869637_7849399
vnoremap <leader>b y:let @"=system('base64 -w 0', @")<cr>gvP
vnoremap <leader>B y:let @"=system('base64 --decode', @")<cr>gvP

"spellin like a wizard
set spelllang=sv,en
nnoremap <leader>son :set spell <ENTER>
nnoremap <leader>sof :set nospell <ENTER>
nnoremap <leader>sf 1z=
nnoremap <leader>sp [s
nnoremap <leader>sn ]s
nnoremap <leader>sa zg
"nnoremap sle todo:set english lang
"nnoremap slv todo:set svenska lang

" Remove trailing whitespaces on selected lines
" (Visual Space)
map <leader>vs :s,\s\+$,,<ENTER>

""" Todo magic
" Disable default key bindings
let g:simple_todo_map_keys = 0
" Map your keys
nmap <leader>o <Plug>(simple-todo-below)
nmap <leader>O <Plug>(simple-todo-above)
nmap <leader>t <Plug>(simple-todo-mark-switch)

"nmap <leader>i <Plug>(simple-todo-new)
"nmap <leader>i <Plug>(simple-todo-new-start-of-line)
nmap <leader>i <Plug>(simple-todo-new-list-item-start-of-line)
"TODO: Create new item in insert mode when current line has a list and I type enter.

" custom macros TODO: REMOVE if not used?
"let @c='yyPiecho "A"'
" let @c='yyPIecho jklys$"'
" let @p='yyPIprintjklys$"ys$)'


" Navigation
" move vertically by visual line
nnoremap j gj
nnoremap k gk
let g:tmux_navigator_no_mappings = 1
nnoremap <silent> <c-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <c-j> :TmuxNavigateDown<cr>
nnoremap <silent> <c-k> :TmuxNavigateUp<cr>
nnoremap <silent> <c-l> :TmuxNavigateRight<cr>
nnoremap <silent> {Previous-Mapping} :TmuxNavigatePrevious<cr>
nnoremap L :bnext<ENTER>
nnoremap H :bprevious<ENTER>

nnoremap <silent> ( gt
nnoremap <silent> ) gT

" Splitting facts
nmap <leader>h :split<ENTER>
nmap <leader>v :vsplit<ENTER>


" TODO: Make them search the most recently used files first somehow (based on
" modify… perhaps… if buffers aren't available)?
" nnoremap <silent> <Leader><Enter> :call FZFBuffers
nnoremap <Leader><Enter> :FZFMru <esc>
nnoremap <Leader>m :FZFBuffers <esc>
" Reeeaaaaallly lovely default :)
nnoremap <C-M> :FzfFilesAndBuffers <esc>


""""""""" SYNTAX, TABS, INDENTATION, AND OTHER NASTIES"""""""""

" HINTS:
" https://arisweedler.medium.com/tab-settings-in-vim-1ea0863c5990
" tabstop: how many columns of whitespace is a \t character worth
" shiftwidth: how many columns of whitespace is a level of indentation worth
" softtabstop: how many columns of whitespace is a tab or backspace worth
" expandtab: use spaces instead of \t chars
" smartindent: smarter indent (mostly) https://vimdoc.sourceforge.net/htmldoc/usr_30.html#30.4
" autoindent: pretty much tells vim to apply the indentation of the current line
"             to the next when pressing `enter` or `o`/`O`.
" Correspondning no<var> unsets the option, e.g. nosmartindent, noexpandtab, etc

"let g:indentLine_char = '¦'

let custom_column_width=80
syntax enable
set synmaxcol=2000 "performance improvement. only syntax for first ### chars.
" &var = var2 syntax is for options https://stackoverflow.com/a/46051262
let &colorcolumn=custom_column_width

filetype plugin on

let my_indentlevel=4
"set noexpandtab
let &softtabstop=my_indentlevel
" On pressing tab, insert 4 spaces
set expandtab "use space instead of tab
set shiftround
"set smarttab "use tab for indentation and space otherwise?
" show existing tab with 4 spaces width
let &tabstop=my_indentlevel
" when indenting with '>', use 4 spaces width
let &shiftwidth=my_indentlevel
"indent stuff
set copyindent
set preserveindent
"autocmd FileType py setlocal nosmartindent "to not mess up new line w/ comment (#) in Python
filetype plugin indent on
"set g:js_indent=/home/lasse/.vim/indent/javascript.vim
" moving around, searching and patterns
"set infercase
set ignorecase
set incsearch
set smartcase

set relativenumber
set number
" folding
"set foldmarker
"set foldmethod=marker
"
set foldlevel=4
set foldmethod=indent
set foldnestmax=10
set nofoldenable

" Linebreak
"set lbr
" tw/textwidth sets automatic new line on this number of characters. Set to 0 to
" disable it.
let &tw=custom_column_width
" autoindent automatically indents good amount after the tw has done a new line
set autoindent
set smartindent

" interesting options but I don't understand them...
"set breakindent
"set breakindentopt=shift:4


" Python-syntax
let g:python_highlight_all = 1
let g:Python3Syntax = 1

"Set syntax on for specific file types
autocmd BufNewFile,BufRead *.gitconfig setlocal syntax=gitconfig
"autocmd FileReadCmd,VimEnter,BufReadPost,BufNewFile,BufRead *.make   set syntax=make
autocmd BufNewFile,BufRead *.make setlocal syntax=make
autocmd FileType make setlocal noexpandtab
" Get the 2-space YAML as the default when hit carriage return after the colon
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd BufEnter,BufNewFile,BufRead *.j2 setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType markdown setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
autocmd FileType text setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
" autocmd BufNewFile,BufRead *.md setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab
" NoMatchParen is there to not highlight parenthesis, because it somehow get's
" screwed up with lsp.
autocmd BufNewFile,BufRead *.md :NoMatchParen
" autocmd BufNewFile,BufRead *.go setlocal tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab

autocmd BufEnter Vagrantfile :setlocal filetype=ruby
autocmd BufEnter Jenkinsfile :setlocal filetype=groovy
"autocmd BufWritePost *.py call Flake8() "run pytest during write... not really that needed when using 'w0rp/ale', and it sorta slows things down during save...

" Add `-` to be part of words in order to support kebab-case.
" Also possible via e.g. https://github.com/chaoren/vim-wordmotion or
" https://github.com/bkad/CamelCaseMotion
autocmd FileType zsh,sh,yaml,markdown,make setlocal iskeyword+=-
" Prevent too big document files from being ale'd (it's very slow sometimes…).
" TODO: Fix it… doesn't seem to work fully… :/
autocmd BufEnter */Documents/*.md if getfsize(expand("%")) > 10000 | ALEDisable | endif

"todo: gör nåt snabbkommando som sätter tab längden till en parameter, typ 2
"eller 4, använd nedan som inspiration:
"autocmd FileType ruby setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2

"set g:js_indent=$HOME/.vim/indent/javascript.vim

""""" Nerdcommenter
" Insert space after comment char: https://stackoverflow.com/a/26042151
let NERDSpaceDelims=1
" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'  " Recommended for e.g. yml/ansible
" Enable NERDCommenterToggle to check all selected lines is commented or not
let g:NERDToggleCheckAllLines = 1


" LSP plugin options
" https://github.com/prabirshrestha/vim-lsp/blob/master/doc/vim-lsp.txt
let g:lsp_diagnostics_echo_cursor = 1  " toggle lsp error message status on bottom row
let g:lsp_document_highlight_enabled = 1  " toggle highlight of symbol under cursor. disable since it seems awkward
let g:lsp_diagnostics_enabled = 1  " toggle diagnostics support totally. disable since it's awkward...
let g:lsp_diagnostics_highlights_enabled = 1
let g:lsp_diagnostics_signs_insert_mode_enabled = 0
" TODO: how the hell to turn off `>A` ....?

"""""""""" TAGS """"""""""
"set tags=./tags;,tags;
"according to http://stackoverflow.com/questions/19330843/how-do-i-automatically-load-a-tag-file-from-a-directory-when-changing-to-that-di, this will make vim 'look for a tags file in the directory of the current file, then upward until / and in the working directory, then upward until /'.
"TODO: ifall i gitrepo, kolla endast upp till roten av repot. om inte repo, kolla endast i samma dir? Tar väldigt lång tid att leta upp till roten känns d som iaf. jag borde kanske alltid kolla ci_tools sist också, då det ligger mycket där.

"GUTENTAG
" enable gtags module
"let g:gutentags_modules = ['ctags', 'gtags_cscope']
"let g:gutentags_modules = ['ctags']
" config project root markers.
"let g:gutentags_project_root = ['.root']
" generate datebases in my cache directory, prevent gtags files polluting my project
"let g:gutentags_cache_dir = expand('~/.cache/tags')
" change focus to quickfix window after search (optional).
"let g:gutentags_plus_switch = 1
"https://robertbasic.com/tags/gutentags/
"let g:gutentags_trace = 1



"""""""""" MISC FUNCTIONS """"""""""

" ! is to override function if it already exists (e.g., when reloading vimrc)
function! Twr()
    :%s/ *\n/\r/g
endfunction

" adds large letter to name
function! Name()
    :execute "normal viwbgU"
endfunction

function! MyColumn(my_width)
    let &colorcolumn=a:my_width
    let &tw=a:my_width
endfunction



""""""""" MISC SETTINGS """""""""
set hlsearch
"set nohlsearch

" Cursor line modifications
let &t_SI = "\<esc>[5 q"  " blinking I-beam in insert mode
let &t_SR = "\<esc>[3 q"  " blinking underline in replace mode
let &t_EI = "\<esc>[ q"  " default cursor (usually blinking block) otherwise
"set cursorcolumn
"set cursorline
"hi CursorLine  cterm=NONE
"hi CursorLine guibg=Grey40
"hi CursorLine  cterm=bold guibg=Grey40
"hi CursorLine term=bold cterm=bold guibg=Grey40


"""mouse ain't good for tmux+iterm...... :*(
set mouse=a "allow mouseclicks to change cursor position
"map <ScrollWheelUp> <C-Y><C-Y><C-Y><C-Y><C-Y>
"map <ScrollWheelDown> <C-E><C-E><C-E><C-E><C-E>
set scrolloff=3 " Keep 3 lines below and above the cursor
"
set lazyredraw          " redraw only when we need to.
"set wildmenu            " visual autocomplete for command menu
"fix backspace not working
set backspace=2
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Turn persistent undo on
"    means that you can undo even when you close a buffer/VIM
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
try
    silent !mkdir -p ~/.vim/undodir
    set undodir=~/.vim/undodir
    set undofile
catch
endtry

#!/usr/bin/zsh
# shellcheck shell=bash
# shellcheck disable=SC2148
#    NOTE: `shell=bash` should probably be zsh … but shellcheck complains about
#          that… so just go with bash to make it stop crying.

##########################
# --- MISC VARIABLES --- #
##########################

export REPOS_DIR=$HOME/repos
if [[ -d "${REPOS_DIR}/bitbucket.org/robin_karlsson/dotfiles" ]]; then
    export DOTFILES_DIR="${REPOS_DIR}/bitbucket.org/robin_karlsson/dotfiles"
else
    export DOTFILES_DIR="${REPOS_DIR}/dotfiles"
fi
export BAK_DIR=$HOME/.bak
export ZSH_RC=$HOME/.zshrc
export VIM_RC=${HOME}/.vimrc

# path stuff for other bins
pathadd "${HOME}/bin"
pathadd "${DOTFILES_DIR}/ro_bins"

# DOCKER_HOST is needed on windows, but prolly not on other OS
#export DOCKER_HOST=localhost:2375
unset DOCKER_HOST

#############################
# --- BLUETOOTH ALIASES --- #
#############################
# TODO: Port to a script so it's useable from e.g. xbindscripts.
bt() {
    # Args:
    # 1. cmd (connect/disconnect/… etc)
    # 2. device (either in the list or a regex towards previously paired ones)
    local bt_cmd
    local card_profile_nr
    local cmd
    local dev
    local mac
    local output

    declare -A devs
    devs[harmon]="04:21:44:CB:42:9C"
    devs[bose]="78:2B:64:CC:0C:BF"
    if [[ -n "$1" ]]; then
        cmd="${1}"
    fi
    dev="harmon"
    mac=${devs[$dev]}
    if [[ -n "$2" ]]; then
        # Verify the device is a key in the associative array
        if ! [[ -v devs[$2] ]]; then
            echo "WARNING: '$2' is not in devs. Will try to parse" >&2
            # Do a search for already paired devs
            if [[ "$(bluetoothctl paired-devices | grep -c -i "$2")" -eq 1 ]]; then
                # TODO: Fix dis
                mac=$(bluetoothctl paired-devices | grep -i "$2" | awk '{print $2}')
            else
                echo "ERROR: '$2' is not in devs and couldn't be parsed" >&2
                return 1
            fi
        else
            dev="${2}"
            mac=${devs[$dev]}
        fi
    fi
    # TODO: Add a unit tests (just needs substring `test` in it)
    bt_cmd="bluetoothctl $cmd $mac"
    # TODO: let toggle figure out device if not specified like 'bt toggle'?
    if [[ "$cmd" =~ 'toggle' || "$cmd" = "t" ]]; then
        output=$(bluetoothctl info "$mac")
        if [[ $output =~ 'Connected: yes' ]]; then
            bt_cmd="bluetoothctl disconnect $mac"
        else
            bt_cmd="bluetoothctl connect $mac"
        fi
    fi
    if [[ $cmd =~ 'test' ]]; then
        echo "$bt_cmd"
        return 0
    fi

    if [[ $(awk '{print $2}' <<<"$bt_cmd") = connect ]]; then
        # Let's only connect if it's not connected, since bluetoothctl connect
        # will hang if it's already connected…
        if bluetoothctl info "$mac" | grep 'Connected: yes' &>/dev/null; then
            echo "INFO: Skipping to connect to '$mac' since it's already connected."
        else
            eval "$bt_cmd"
        fi
    else
        eval "$bt_cmd"
    fi

    # TODO: Fix this … no more pulsedaudio no more… (probably pipwire instead
    # since Ubuntu 22->24 upgrade…)
    # If we are connected then I want to default to the a2dp profile
    # (if I want another profile then just change it in the GUI).
    if bluetoothctl info "$mac" | grep 'Connected: yes' &>/dev/null; then
        echo "INFO: Setting a2dp profile for bluetooth dev"
        card_profile_nr="$(pacmd list-cards | grep -iC5 bluez |
            grep 'index: ' | grep -oP '\d+')"
        pacmd set-card-profile "$card_profile_nr" a2dp_sink
    fi
}

########################
# --- MISC ALIASES --- #
########################

alias grep='grep --color=tty -d skip'
alias cp='cp -i' # confirm before overwriting something

# cd aliases and commonly used directories
rep() {
    local repo
    repo=${1}
    #shellcheck disable=SC2164
    cd "${REPOS_DIR}/${repo}"
}
work() {
    local subdir WORK_DIR
    subdir=${1}
    WORK_DIR=$HOME/work
    mkdir -p "${WORK_DIR}"
    #shellcheck disable=SC2164
    cd "${WORK_DIR}/${subdir}"
}
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias repos='cd ${REPOS_DIR} '
alias rd='cd ${DOTFILES_DIR}'
alias dl='cd ~/Downloads'
alias doc='cd ~/Documents'
alias t='cd /tmp'
# Some nice ergonomics to skip left hand
alias åå='exit'
alias öö='exit'

# reload this file from a shell
alias re='. $ZSH_RC'

# vim
alias v='vim'
alias i='vim'
# alias vim='vim '
alias vr='vim $VIM_RC'
alias vf='vim $(fzf)'
# https://lyz-code.github.io/yamlfix/#sequence-list-style
export YAMLFIX_SEQUENCE_STYLE="keep_style" # For yamlfix (also in vim via ALE).
export YAMLFIX_preserve_quotes="true"

# ls improvements
alias la='ls -a '
alias l='ls -lahtr '
alias ll='ls -lahtr '

# tmux aliases/functions
_tmux() {
    local name
    if [[ -n "$1" ]]; then
        export name="$1"
    else
        export name='main'
    fi
    if tmux list-sessions | grep -Eq "^$name:"; then
        tmux attach -d -t "$name"
    else
        tmux new -s "$name"
    fi
}
alias ta='_tmux'
alias tl='tmux list-sessions '

# misc
alias df='df -h '
alias speedtest='wget --output-document=/dev/null http://speedtest.wdc01.softlayer.com/downloads/test500.zip'
alias hl='history | less '
alias avv='ansible-vault view '
alias ave='ansible-vault edit '
# Plz dont kill the poor ssh session with ServerAliveInterval.
alias ssh='ssh -o ServerAliveInterval=60 '

alias ctop='docker run --rm -ti -v /var/run/docker.sock:/var/run/docker.sock quay.io/vektorlab/ctop:latest'
alias ipmantables='sudo iptables -L -n -t nat --line-number'
alias view-cert='openssl x509 -noout -text -in '
alias pixels='ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0'

# Copy to clipboard from either stdin or from a file.
# Meaning e.g. `echo | cin` or `cin asdf.yaml` or `<README.md cin`.
cin() {
    local file
    if [[ -n "$1" ]]; then
        file=$1
        xclip -sel clipboard -in "${file}"
    else
        xclip -sel clipboard -in
    fi
}

# Perhaps skip this one and use `cin` instead?
gbf() {
    local pre_command branch
    pre_command="${1:-gco}"
    branch="$(gb | fzf)"
    echo "$branch" | xclip -i -sel clip
    print -z "$pre_command $branch"
}

dus() {
    local dir
    dir=.
    if [[ -n ${1} ]]; then
        dir=${1}
    fi
    # Nice to also check disk usage for hidden files (exclude . and ..)
    if find "$dir" -name '.*' -maxdepth 1 2>/dev/null | grep -q '/'; then
        # shellcheck disable=SC2086
        du -hcs ${dir}/* ${dir}/.* | sort -h
    else
        # shellcheck disable=SC2086
        du -hcs ${dir}/* | sort -h
    fi
}

# TODO: fix for files/dirs with space in the name.
lbak() {
    test -d "$BAK_DIR" || mkdir -p "$BAK_DIR"
    for src in "${@}"; do
        cp -r "$src" "$BAK_DIR/$(basename "$src").$(date +%Y-%m-%dT%H_%M_%S).bak"
    done
}

# Nice wrapper for quick shell linting during prototyping
shlint() {
    local file
    # TODO: Support putting in a dir as arg and running shellcheck + shfmt
    # recursively on all files
    file="${1}"
    shellcheck "$file" && shfmt -i 4 -d "$file"
    return "$?"
}

# Start a super simple http server
http-serve() {
    local port
    if [[ $# -gt 0 ]]; then
        export port=$1
    else
        export port=8000
    fi
    python3 -m http.server "${port}"
}

removeTrailingWhitespace() {
    local file
    file=$1
    sed -i "s/[ ]*$//g" "$file"
}

fixFilenames() {
    # shellcheck disable=SC2046
    # shellcheck disable=SC2001
    for file in "$@"; do
        if [[ -f "$file" || -d "$file" ]]; then
            mv "$file" "$(echo "$file" | sed 's, ,.,g')"
        fi
    done
}

unalias tmp &>/dev/null || true
export scratch_dir="/tmp/.dotfiles_scratch_area"
tmpnew() {
    ln -Tsf "$(mktemp -d --suffix='.dotfiles')" "${scratch_dir}"
}

tmp_check() {
    if ! file -E "${scratch_dir}" &>/dev/null; then
        tmpnew
    fi
}

tmp() {
    tmp_check
    # shellcheck disable=SC2164
    cd "$(readlink -f "${scratch_dir}")"
}

tmpcp() {
    tmp_check
    cp -nr "$@" "${scratch_dir}/"
}

tmpmv() {
    tmp_check
    mv "$@" "${scratch_dir}/"
}

# Vim command
vc() {
    local a cmd
    a=$1
    cmd="$(which "$a")"
    if [ -f "$cmd" ]; then
        vim "$cmd"
    else
        echo "$cmd"
    fi
}

#####################
# --- k8s STUFF --- #
#####################

# Kubernetes related (only load if kubectl is installed).
if which kubectl &>/dev/null; then
    # Don't re-source if it's already sourced:
    # Probably ZSH_KUBECTL_COMPLETION should not be exported, because then it
    # will be inherited to subshells and tmux sessions etc, even though the
    # sourced completion is not inherited… So then we can also check
    # `__kubectl_debug` since that's defined only in the completion env.
    # local kubectl_completion
    if ! which __kubectl_debug &>/dev/null ||
        [[ "${ZSH_KUBECTL_COMPLETION:-}" != true ]]; then
        kubectl_completion=/tmp/.kubectl-zsh-autocompletion
        kubectl completion zsh >"${kubectl_completion}"
        # This magic will enable zsh autocompletion for my `k` function (default
        # is only `kubectl`)
        sed -i 's,^compdef _kubectl kubectl$,compdef _kubectl kubectl k,g' "${kubectl_completion}"
        # shellcheck disable=SC1090
        source "${kubectl_completion}"
        ZSH_KUBECTL_COMPLETION="true"
    fi

    # TODO: Improve this function (it's kinda semi-tested only). Seems like a
    # good idea prevent deleting stuff in prod environment.
    k() {
        # TODO: … make autocompletion work… … it's kinda shit as is since it
        # doesn't work with tab………
        # if [[ "$*" =~ delete ]]; then
        #     if ! kenv |& grep -E 'admin.conf|development' &>/dev/null; then
        #         echo "Are you sure you want to run '$*' towards non-dev…?"
        #         kenv
        #         read -r "REPLY? [y/N]? "
        #         if ! [[ $REPLY =~ ^[Yy]$ ]]; then
        #             return 1
        #         fi
        #     fi
        # fi
        kubectl "$@"
    }
    # Get events. There are some different approaches, as described in
    # https://stackoverflow.com/questions/45226732/what-kubectl-command-can-i-use-to-get-events-sorted-by-specific-fields-and-print
    # --sort-by=".lastTimestamp"' gets stuff of different types (like pods and
    # FailedScheduling) in weird order…
    # Creationtimestamp is not always good either since old pods (e.g. servers)
    # have very old such timestamp, but can still have new events.
    # idk exactly what managefileds[0].time refers to…, but order seems decent
    # alias kevents='kubectl get events --sort-by=".lastTimestamp"'
    # alias kevents='kubectl get events --sort-by=".metadata.creationTimestamp"'
    # alias kevents='kubectl get event --sort-by=".metadata.managedFields[0].time"'
    alias kevents='kubectl events'

    alias kg='kubectl get '
    alias kgp='kubectl get pod '
    alias kn='kubectl get nodes -o wide'
    alias kd='kubectl describe '
    alias kdp='kubectl describe pod '
    alias kl='kubectl logs '
    kns() {
        local namespace
        namespace="${1}"
        if [[ -z "${namespace:-}" ]]; then
            echo "ERROR: No namespace specified…" >&2
            # namespace=default
            return 1
        fi
        kubectl config set-context --current --namespace="${namespace}"
    }

    kpods() {
        local node
        node="${1}"
        kubectl get pods --all-namespaces -o wide \
            --field-selector spec.nodeName="${node}"
    }
    # shellcheck disable=SC2120
    kenv() {
        local base_config
        local env
        local pat
        env="${1}"
        base_config="$HOME/.kube/config"
        pat="^(prod|production|staging|dev|development)$"
        if [[ -z "${env}" ]]; then
            echo "Current config points to: $(readlink -f "${base_config}")"
            return 0
        elif ! [[ "${env}" =~ ${pat} ]]; then
            echo "ERROR: ${env} not in '${pat}'!" >&2
            return 1
        fi
        ln -sf "${base_config}.${env}" "${base_config}"
    }
fi

#####################
# --- GIT STUFF --- #
#####################

# gb better git branch
alias gb='git for-each-ref --format="%(refname:short)" --sort=-committerdate refs/heads/ refs/remotes/origin/'
# gitk with all branches
alias gk='gitk --all&'
# better git log
alias gl='git log --pretty=tformat:"%x09%C(auto)%h %ad| %<(72,trunc)%s%C(dim white)[%an]%Creset%C(auto)%d%N" --graph --date=short'
# pretty print path
alias pp='echo $PATH | tr ":" "\n"'
# nicer git status
alias gs='git status -sb'
alias gst='git stash '
alias gstp='git stash pop '

alias grb='git rebase -i origin/$(get-remote-head)'
alias gco='git checkout '
alias gcp='git cherry-pick '
gp() {
    local branch remote remote_head
    # remote="${1:-origin}"
    remote="origin"
    branch="$(git rev-parse --abbrev-ref HEAD)"
    remote_head="$(get-remote-head "$remote")"
    # TODO: check multiple branches?
    if [[ "$branch" = HEAD ]]; then
        echo "ERROR: You're not on a local branch (it shows HEAD)… skipping push…" >&2
        return 1
    elif [[ "$branch" =~ $remote_head ]]; then
        echo "ERROR: You're trying to push directly to the default branch, let's not do that right now…" >&2
        return 2
    fi
    git push "$remote" "$branch" "$@"
}
alias gc='gcd'
get-fq-path() {
    local url fq_path tmp
    url="$1"
    # HINT: https://devhints.io/bash#parameter-expansions
    tmp=$url
    fq_path=${tmp##*//}
    tmp=$fq_path
    fq_path=${tmp##*@}
    tmp=$fq_path
    # shellcheck disable=SC2001
    fq_path="$(echo "${tmp}" | sed 's,:[0-9]\+,,')"
    tmp=$fq_path
    # shellcheck disable=SC2001
    fq_path="$(echo "${tmp}" | sed 's,:,/,')"
    tmp=$fq_path
    fq_path=${tmp%.git}
    echo "$fq_path"
}
gcf() {
    local url fqdir
    url="$1"
    shift 1
    if [[ -z "$url" ]]; then
        echo No URL specified… >&2
        return 1
    fi
    fqdir="$(get-fq-path "$url")"
    mkdir -p "$fqdir"
    git clone "$url" "$fqdir" "$@"
}
gcd() {
    local url fqdir
    url="$1"
    shift 1
    if [[ -z "$url" ]]; then
        echo No URL specified… >&2
        return 1
    fi
    fqdir="$(get-fq-path "$url")"
    if ! [ -d "$fqdir" ]; then
        gcf "$url" "$@"
    fi
    cd "$fqdir" || return 1
}
cdf() {
    local url fqdir
    url="$1"
    if [[ -z "$url" ]]; then
        echo No URL specified… >&2
        return 1
    fi
    cd "$(get-fq-path "$url")" || return 1
}
alias gcoh='git checkout HEAD '
unalias gcom &>/dev/null || true
get-remote-head() {
    # Finds default branch (typically used for origin/HEAD points at)
    local branch remote file full_branch
    remote="${1:-origin}"

    # This multiple checking is theer just becausea doing a `git remote show …`
    # is soo sloow. We could also skip these checks and simply use some file in
    # the .git directory?
    # local remote_ref
    # remote_ref="master"
    # if ! git show-ref $remote/$remote_ref &>/dev/null; then
    #     remote_ref="master"
    #     if ! git show-ref $remote/$remote_ref &>/dev/null; then
    #         remote_ref="main"
    #         if ! git show-ref $remote/$remote_ref &>/dev/null; then
    #             # return 1
    #         fi
    #     fi
    # fi

    file="$(git rev-parse --show-toplevel)/.git/refs/remotes/$remote/HEAD"
    # echo $file
    if [[ -f "$file" ]]; then
        full_branch=$(awk <"$file" '{print $NF}')
        # echo $full_branch
        branch="${full_branch/refs\/remotes\/$remote\//}"
        # echo $branch
    fi

    if [[ -z "$branch" ]]; then
        branch="$(git remote show "$remote" | sed -n '/HEAD branch/s/.*: //p')"
        if [[ -z "$branch" ]]; then
            echo "ERROR: Couldn't figure out remote branch…"
            return 1
        fi
    fi

    echo "$branch"
}
gcom() {
    # Checks out the default branch (the one origin/HEAD points at)
    local default_branch remote
    remote="${1:-origin}"
    default_branch="$(get-remote-head "$@")"
    git checkout "$default_branch"
}

# alias gcm='git commit -m '
unalias gcm &>/dev/null || true
gcm() {
    local msg
    msg='Stuff'
    if [[ $# -gt 0 ]]; then
        msg="$*"
    fi
    git commit -m "$msg"
}
gcmf() {
    local commit_ref
    # TODO: Figure out previous commit which doesn't start with "fixup!"
    if [[ -n $1 ]]; then
        commit_ref="${1}"
    else
        # NOTE: Mega parameters copied from glf, might not be optimal but it
        # works :)
        # TODO: Why is the first column messed up compared to glf? Fix.
        commit_ref="$(git log --date=short --color=always --oneline \
            --pretty=tformat:"%x09%C(auto)%h %ad| %<(72,trunc)%s%C(dim white)[%an]%Creset%C(auto)%d%N" "$@" |
            fzf --ansi --no-sort --reverse --tiebreak=index --bind=ctrl-s:toggle-sort |
            grep -Po '[a-f0-9]{7,}')"
    fi
    git commit --fixup="$commit_ref"
}
alias lg='lazygit'
alias gca='git commit --amend '
alias gf='git fetch --all '
alias gfrb='git fetch --all && git rebase origin/HEAD '
alias gsu='git submodule update --init --recursive '
alias gd='git diff '
alias gds='git diff --staged '
alias gsh='git show '
alias gg='git grep '
alias glg='git ls-files | grep '
alias ga='git add '
alias gau='git add -u '
alias gr='GIT_ROOT=`git rev-parse --show-toplevel` && cd "$GIT_ROOT"'
alias gpr='git pull --rebase'

alias gsize='git count-objects -vH'
alias my_commits='gl --author="Robin Karlsson"'

# ff() {
#     local input file
#     while read input; do
#         echo "$input"
#     done | fzf | sed -nr 's,(^[^:|\S]+):.*,\1,p' | read file
#     if [[ -n "$file" ]]; then
#         echo "$file" | xclip -i -sel clip
#         print -z "$file"
#     else
#         return 1
#     fi
# }

# TODO: Rename or? … and maybe unite with ff? Make a list of use cases!
# - pipe from stdin XOR take from cli arg
#   - if no arg, search from stdin, but if no stdin, search all strings?
#   - if stdin, do that. Other wise search from arg, but if no arg then search
#     all?
# - print file to next line and copy to clipboard?
# - open file im vim?
# TODO: Look into the glf way of executing the result.
vff() {
    local file search_string
    search_string="${1}"
    # if [[ -z "$search_string" ]]; then
    #     # Let's read input then if nothing was given already then
    #     # read search_string
    #     while read input; do
    #         echo "$input"
    #     done | fzf | sed -nr 's,(^[^:|\S]+):.*,\1,p' | read file
    # else
    # file=$(git grep -i "$search_string" | fzf | sed -nr 's,(^\S+):.*,\1,p')
    file=$(ack --smart-case "$search_string" | fzf |
        sed -nr 's,(^[^:|\S]+):.*,\1,p')
    # fi
    if [[ -n "$file" ]]; then
        vim "$file"
    else
        return 1
    fi
}

# Outputs the oldest shared commit between HEAD and a branch
gbranchoff() {
    local other_branch
    other_branch=${1:-origin/HEAD}
    # <() stopped working in zsh with error. Changing to =() works although it's
    # zsh specific and not portable. It also only fails on color-diff. So let's
    # use diff and not as a wrapper for color-diff!
    /bin/diff -u <(git rev-list --first-parent HEAD) \
        <(git rev-list --first-parent "$other_branch") |
        sed -ne 's/^ //p' | head -1
}

# creates a backup git branch of current commit
gbak() {
    local new_branch old_branch bak_nr
    # old_branch=$(git branch 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
    old_branch=$(git rev-parse --abbrev-ref HEAD)
    # shellcheck disable=SC2086
    for bak_nr in {1..99}; do
        new_branch="${old_branch}_${bak_nr}"
        if ! git branch -av | grep " ${new_branch} " >/dev/null; then
            git branch "$new_branch"
            echo "Created branch '$new_branch'"
            break
        fi
        if [[ $bak_nr -eq 99 ]]; then
            echo "Failed to create branch '$new_branch'" >&2
            return 1
        fi
    done
}

# Outputs changes in current local branch compared to it's corresponding remote
gdremote() {
    local remote
    local branch

    if [[ "${#}" -lt 2 ]]; then
        echo "Error: Need both remote and branch" >&2
        return 1
    fi
    remote="$1"
    branch="$2"
    shift 2
    git fetch "${remote}" >/dev/null # Fetch output confuses… (skip stderr?)…
    git diff "${remote}/${branch}" "${branch}" "$@"
}

# Outputs changes in current local branch compared origin
gdorigin() {
    local branch
    if [[ -n "$1" ]]; then
        branch="${1}"
        shift
    else
        branch="$(git rev-parse --abbrev-ref HEAD)"
    fi
    gdremote origin "${branch}" "$@"
}
alias gdo='gdorigin'
alias gdob='gdorigin'
gdoh() {
    # local default_branch
    # remote="${1:-origin}"
    # default_branch="$(get-remote-head "$@")"
    git diff "origin/$(get-remote-head origin)" HEAD "$@"
}

# TODO: Remove dis
gdoh2() {
    local origin_ref
    remote="${1:-origin}"
    origin_ref="HEAD"
    if ! git show-ref origin/$origin_ref &>/dev/null; then
        origin_ref="master"
        if ! git show-ref origin/$origin_ref &>/dev/null; then
            origin_ref="main"
            if ! git show-ref origin/$origin_ref &>/dev/null; then
                return 1
            fi
        fi
    fi
    git diff origin/$origin_ref HEAD "$@"
}

gmb() {
    # Git move local branch pointer to to origin version
    local branch_to_move current_branch target_ref remote
    remote="origin"
    current_branch="$(git rev-parse --abbrev-ref HEAD)"
    branch_to_move="$current_branch"
    if [[ $# -eq 1 ]]; then
        branch_to_move="$1"
    fi
    target_ref="$remote/$branch_to_move"
    if [[ $# -ge 2 ]]; then
        target_ref="$2"
    fi
    # target_ref="$(git show-ref --hash "$target_ref")"
    # target_sha="$(git rev-parse "$target_ref")"
    # TODO: fix this function and make it reliable :)

    # e.g. `git fetch origin wip/nice-feature`
    git fetch "$remote" "${target_ref#"$remote/"}" &>/dev/null
    if [[ "$branch_to_move" = "$current_branch" ]]; then
        git checkout "$target_ref" >/dev/null &&
            git branch -f "$branch_to_move" "$target_ref" &&
            git checkout "$branch_to_move"
    else
        git branch -f "$branch_to_move" "$target_ref" &&
            git checkout "$branch_to_move"
    fi
}

glf() {
    # alias gl='git log --pretty=tformat:"%x09%C(auto)%h %ad| %<(72,trunc)%s%C(dim white)[%an]%Creset%C(auto)%d%N" --graph --date=short'
    # --format="%C(auto)%h%d %s %C(black)%C(bold)%cr" "$@" |
    git log --graph --date=short --color=always \
        --pretty=tformat:"%x09%C(auto)%h %ad| %<(72,trunc)%s%C(dim white)[%an]%Creset%C(auto)%d%N" "$@" |
        fzf --ansi --no-sort --reverse --tiebreak=index --bind=ctrl-s:toggle-sort \
            --bind "ctrl-m:execute:
                (grep -o '[a-f0-9]\{7\}' | head -1 |
                xargs -I % sh -c 'git show --color=always % | less -R') << 'FZF-EOF'
                {}
FZF-EOF"
}
alias glfa='glf --all'
alias glaf='glf --all'

#########################
# --- END GIT STUFF --- #
#########################

randchars() {
    local length
    length="${1:-20}"
    # Too many weird characters to escape in the CLI…
    # tr -dc 'A-Za-z0-9#%&()*+-<=>[\]^_' </dev/urandom |
    # tr -dc 'A-Za-z0-9!#$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' </dev/urandom |
    #     head -c "${length}"
    # base64 </dev/urandom | head -c "${length}"
    # head -c 4096 /dev/urandom | sha256sum | base64 | head -c "${length}"

    # Generate some random base64-ish string, but ignore `+`, `/`, and
    # concatenate so it's cleaner. I add a few chars to the password to
    # compensate for this (4+l/10).
    # Entropy per char is then 62 instead of 64, which is fine.
    openssl rand -base64 "$((length + 4 + length / 10))" | tr -d '+/\n' |
        head -c "${length}"
    echo
}

# TODO: Start using gpg
encrypt() {
    local file
    file=$1
    echo 'Todo: fix broken skipped: no public key…'
    # https://tinyapps.org/blog/201705300700_gpg_without_keys.html
    set -x
    # gpg --pinentry-mode loopback --symmetric --armor --cipher-algo AES256 --encrypt -R "$file"
    gpg --symmetric --armor --cipher-algo AES256 --encrypt -R "$file"
    set +x
}
decrypt() {
    local file
    file=$1
    echo 'Todo: Use gpg'
}

# NOTE: Don't use openssl for encryption, it's not supersafe…
# https://stackoverflow.com/questions/28247821/openssl-vs-gpg-for-encrypting-off-site-backups
# https://security.stackexchange.com/questions/29106/openssl-recover-key-and-iv-by-passphrase/29139#29139
# https://security.stackexchange.com/questions/182277/is-openssl-aes-256-cbc-encryption-safe-for-offsite-backup
#e.g. openssl enc -d -aes256 -in docs_etc.210714.0915.tar.bz2.aes256 >docs_etc.210714.0915.tar.bz2
encrypt-openssl() {
    echo openssl enc -d -aes256 -in "${1}" '>'"${1}.aes256"
    openssl enc -e -aes256 -in "${1}" >"${1}.aes256"
}
decrypt-openssl() {
    local arg
    arg=$1
    openssl enc -d -aes256 -in "${arg}" >"${arg/%.aes256/}"
}

alias wssh='waitssh'
alias ws='waitssh'
waitssh() {
    ssh "$@"
    while test $? -gt 0; do
        echo "Trying again..."
        sleep 5 # highly recommended - if it's in your local network, it can try an awful lot pretty quick...
        ssh "$@"
    done
}

j() {
    # Inspired by https://github.com/junegunn/fzf/wiki/examples#autojump
    if [[ $# -eq 0 ]]; then
        # shellcheck disable=SC2164
        cd "$(autojump -s | sort -k1gr | awk '$1 ~ /[0-9]:/ && $2 ~ /^\// {print $1 " " $2}' | fzf --height 40% --reverse --inline-info --preview-window down:50% | awk '{print $2}')"
    else
        # shellcheck disable=SC2164
        cd "$(autojump "$@")"
    fi
    # Hint. Run `autojump --purge` every now and then to purge old non-existing
    # paths from the autojump database.
    # `fzf +s` is also an interesting option to add that sorts candidates
}

dr() {
    local mountPath
    mountPath="/myPWD"
    sudo docker run -it "$@"
}

drl() {
    local mountPath
    mountPath="/myPWD"
    sudo docker run -it -v "$PWD:$mountPath" -w "$mountPath" "$@"
}

uplift-image() {
    local image newtag
    image="${1}"
    newtag="${2}"
    # Perhaps check if we're in a repo or not, and use find if not?
    if git rev-parse --abbrev-ref HEAD &>/dev/null; then
        # git grep --files-with-matches $image | xargs sed -i -E "s,${image}:\S*,${image}:$newtag,g"
        git grep --files-with-matches "${image}"
    else
        # find with -exec sed … is super slow for many files, it's much faster
        # to pipe all files in to xargs and then call sed only one time for all.
        find . -type f
    fi | xargs sed -i -E "s,${image}:\S*,${image}:${newtag},g"
}

alias ggf='git grep --files-with-matches'
ggfr() {
    local oldstr newstr
    oldstr="${1}"
    newstr="${2}"
    # Perhaps check if we're in a repo or not, and use find if not?
    if git rev-parse --abbrev-ref HEAD &>/dev/null; then
        # git grep --files-with-matches $image | xargs sed -i -E "s,${image}:\S*,${image}:$newtag,g"
        git grep --files-with-matches "${oldstr}"
    else
        # find with -exec sed … is super slow for many files, it's much faster
        # to pipe all files in to xargs and then call sed only one time for all.
        find . -type f
    fi | xargs sed -i -E "s|${oldstr}|${newstr}|g"
}

fp-link() {
    local app
    app="$1"
    fp "$app" -h >/dev/null 2>&1 || { echo "App pattern '$app' not found…" && return 1; }
    mkdir -p "$HOME/bin/"
    cat >"$HOME/bin/$app" <<EOF
#!/usr/bin/env bash

exec fp $app "\$@"
EOF
    chmod +x "$HOME/bin/$app"
}

###################################
# --- Example code generators --- #
###################################

bash-example() {
    local script
    script="${1:-bash-template.sh}"
    cp "${DOTFILES_DIR}/files/bash-template.sh" "${script}"
    chmod +x "${script}"
}

go-init() {
    local target_path
    target_path="${1:-}"
    if [[ -z $target_path ]]; then
        target_path="."
    fi
    # TODO: Do some kind of dirname operation on the target_path var?
    cp --update=none "${DOTFILES_DIR}"/files/go/{main.go,main_test.go,Makefile} "${target_path}"
}

python-example() {
    local script
    script="${1:-python_template.py}"
    cp "${DOTFILES_DIR}/files/python_template.py" "${script}"
    chmod +x "${script}"
}

ansible-playbook-example() {
    local file
    file="${1:-example-playbook.yml}"
    test -f "$file" && return 1
    # NOTE: The `-S` for multiple env parameters may not be 100% portable, but
    # it seems to work fine on all of my systems.
    cat <<EOF >"$file"
#!/usr/bin/env -S ansible-playbook -i /dev/null

---
- name: Setup local environment
  hosts: localhost
  connection: local
  vars:
    example_var: "sigh…"
  tasks:
    - name: "Example"
      ansible.builtin.debug:
        msg: "{{ example_var }}"
EOF
    chmod +x "$file"
}

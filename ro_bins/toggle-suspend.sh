#!/usr/bin/env bash

set -euo pipefail

pid_file=/tmp/.toggle-suspend-pid
pre_sleep=1.5

cleanup() {
    rm $pid_file || true
}
trap cleanup EXIT

if [[ -f $pid_file ]]; then
    kill "$(cat "$pid_file")"
    wall 'Cancelled the old suspend process'
    exit 0
else
    echo "$$" >"$pid_file"
fi

try_lock() {
    # dbus-send --type=method_call --dest=org.gnome.ScreenSaver /org/gnome/ScreenSaver org.gnome.ScreenSaver.Lock
    # nohup bash -c '{ sleep 1; loginctl lock-session ;}' &

    # NOTE: the sleep is kinda needed in order to not block lock, meaning if I
    # don't add it in a nohup subprocess it will not lock when called from
    # xbindkeys due errors looking like: `gnome-shell[2884160]: error: Unable to
    # lock: Lock was blocked by an application`
    nohup bash -c '{ sleep 0.1; xdg-screensaver lock ;}' &
}

sleep_stuff() {
    wall 'Will suspend in a few seconds'

    sleep $pre_sleep
    try_lock
    sleep 10
    systemctl suspend
}
sleep_stuff

#!/usr/bin/env bash

set -uo pipefail

file=$(mktemp)
trap 'rm $file' EXIT
days=32
if [[ $# -gt 0 ]]; then
    days="$1"
fi
# TODO: Run sudo as well or smth
journalctl -q --since "$days days ago" >"${file}"
# TODO: Look in the zshhistory and bash history as well.

# TODO: Just go through the journal file one time as it's more optimal
# Loop through each month just to get output in order.
for month in jan feb mar apr maj jun jul aug sep okt nov dec; do
    for date in {01..31}; do
        # NOTE: piping stdout into multiple commands like this is not correct
        # grep -E "^\\S+ ${date}" "${file}" | (
        #     head -n 1
        #     tail -n 1
        # )
        grep -E "^$month ${date}" "${file}" | head -n 1
        grep -E "^$month ${date}" "${file}" | tail -n 1
    done
done

echo
echo "Generated: $(date)"

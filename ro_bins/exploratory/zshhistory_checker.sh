#!/usr/bin/env bash

set -x
set -euo pipefail

# SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"
# NOTE: should only be filename and no path because of find hack …
last_gc_file=.last-gc-timestamp
user="${1:-$USER}"
bak_dir="/home/$user/.zsh_bak/"
active_file="/home/$user/.zsh_history"
latest_bak_file="$bak_dir/.zsh_history"
maximum_bak_file="$bak_dir/.zsh_history_maximum"

# Will create the directory and all its parent if it doesn't already exist.
# Running git-init inside an already existing repo is safe
# https://git-scm.com/docs/git-init
git init -q "$bak_dir"
[ ! -f "$latest_bak_file" ] && cp "$active_file" "$latest_bak_file"
[ ! -f "$maximum_bak_file" ] && cp "$active_file" "$maximum_bak_file"
[[ -f $SCRIPT_DIR/$last_gc_file ]] || touch "$SCRIPT_DIR/$last_gc_file"

doStuff() {
    active_size="$(wc -l "$active_file" | cut -d' ' -f1)"
    latest_bak_size="$(wc -l "$latest_bak_file" | cut -d' ' -f1)"
    maximum_bak_size="$(wc -l "$maximum_bak_file" | cut -d' ' -f1)"

    echo "active_size: $active_size, latest_bak_size: $latest_bak_size, maximum_bak_size: $maximum_bak_size"

    if [ "$active_size" -eq "$latest_bak_size" ]; then
        echo Equal ... pass
        return 0
    elif [ "$active_size" -gt "$latest_bak_size" ]; then
        echo New is greater than old... that is good i guess?
    elif [ "$active_size" -lt "$latest_bak_size" ]; then
        echo New is lesser than old... crap
        zenity --info --text="Zsh history was just decreased :/....."
    fi

    cp -f "${active_file}" "${latest_bak_file}"

    if [ "$active_size" -gt "$maximum_bak_size" ]; then
        echo Latest is greater than the maximum bak file. Copying.
        cp -f "${active_file}" "${maximum_bak_file}"
    fi
    (
        cd "$bak_dir"
        git add . && git commit -m 'backup'
        sparseGC
    )
}

sparseGC() {
    # Only run git gc every now and then (once every day)!
    # A bit ugly with the find command, but it's so easy to compare date
    find "$SCRIPT_DIR" -maxdepth 1 -name "$last_gc_file" -mtime +1 -exec \
        bash -c "echo 'Running gc'; git gc && touch $SCRIPT_DIR/$last_gc_file" \; -quit
}

while true; do
    doStuff
    sleep 5s
done

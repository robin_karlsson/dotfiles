#!/usr/bin/env bash

trap 'rm $dockerfile' EXIT

dockerfile=$(mktemp)
image_name=bredbandskollen
if [[ -z "$(docker images -q "$image_name")" ]]; then
    echo "Couldn't find image '$image_name' locally, will build it!"
    cat <<EOF >"$dockerfile"
FROM ubuntu:22.04
RUN apt update -y && apt install -y wget git make g++
RUN git clone https://github.com/dotse/bbk.git \
    && cd bbk/src/cli \
    && make \
    && cp cli /tmp
EOF
    chmod 666 "$dockerfile"
    docker build -t "$image_name" -f "$dockerfile" .
fi
docker run -t "$image_name" /tmp/cli

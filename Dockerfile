FROM ubuntu:22.04

# noninteractive is needed to skip setting timezone and stuff manually
RUN apt update && \
    DEBIAN_FRONTEND=noninteractive apt install -y make \
                                                  ansible \
                                                  git

# TODO: Remove git install and set this config in ansible possibly or?
RUN git config --global --add safe.directory /src

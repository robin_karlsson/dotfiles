#!/usr/bin/env zsh
# shellcheck shell=bash

set -euo pipefail

# shellcheck disable=SC1091
source dotfiles/.zshrc

run-test() {
    local expected received
    expected="$1"
    received="$2"
    echo "Checking if ${expected}==${received}!"
    if [[ "${expected}" != "${received}" ]]; then
        echo "ERROR: '${expected}' != '${received}'" >&2
        return 1
    fi
    return 0
}

run-test "github.com/prometheus-community/ansible" "$(get-fq-path "https://github.com/prometheus-community/ansible.git")"
run-test "github.com/prometheus-community/ansible" "$(get-fq-path "https://user:pw@github.com/prometheus-community/ansible.git")"
run-test "git.example.com/POOP/ansible-node-exporter" "$(get-fq-path "git@git.example.com/POOP/ansible-node-exporter.git")"
run-test "git.example.com/ansible-node-exporter" "$(get-fq-path "git.example.com:1337/ansible-node-exporter.git")"

#!/usr/bin/env bash

set -euo pipefail
echo "######################## Verify commit author ###########################"
exit_code=0
expected_author="robin.s.karlsson@gmail.com"
for commit in $(git log HEAD --not origin/master --pretty=oneline | awk '{print $1}'); do
    author="$(git show -s --format='%ae' "${commit}")"
    if [[ "${author}" != "${expected_author}" ]]; then
        echo "${commit}: ${author} != ${expected_author}"
        exit_code=1
    fi
done
[[ "${exit_code}" -eq 1 ]] && echo NOK! || echo OK!
exit "${exit_code}"

#!/usr/bin/env bash

set -euo pipefail

# TODO: be more stringent with exit code, e.g., wrong use of keywords etc
echo "######### Check if the .zshrc itself is good or not #####################"
zsh -c "source dotfiles/.zshrc"
echo OK!
echo "## The extended .env_public.sh depends on .zshrc, so source that first. #"
zsh -c "source dotfiles/.zshrc && source dotfiles/.env_public.sh"
echo OK!
echo "######################### Unit test shell ###########################"
./tests/zsh_unit_tests.sh

# Note dont use find to execute commands, since it doesn't return exit code of
# the shellcheck https://apple.stackexchange.com/questions/49042/how-do-i-make-find-fail-if-exec-fails
echo "#################### Lint sh files with shellcheck ######################"
find . -name '*.sh' -print0 | xargs -0 shellcheck
echo OK!
echo "################ Check sh files formatting with shfmt ###################"
find . -name '*.sh' -print0 | xargs -0 shfmt -i 4 -d
echo OK!
echo "######################### Yamllint .yml files ###########################"
find . -name '*.yml' -print0 | xargs -0 yamllint
echo OK!
# Hint: format the shellscripts with shfmt -i 4 -w <filename>

# TODO: Verify author name and email is good.

.PHONY: dependencies docker/test docker/image setup setup/workstation test

# Or you can just call ./site.yml
setup: dependencies
	ansible-playbook -K ${vars} site.yml

setup/workstation: dependencies
	ansible-playbook -K ${vars} workstation-playbook.yml

dependencies:
	which ansible-playbook || sudo apt install ansible

test:
	@tests/shell.sh
	@tests/git.sh
	env LC_ALL=en_US.UTF-8 ansible-lint

docker/image:
	sudo docker build -t dotfiles:latest .

docker/test: docker/image
	sudo docker run -v $(PWD):/src -w /src dotfiles:latest make setup test

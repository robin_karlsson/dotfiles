package main

import (
	"strings"
	"testing"
	"fmt"
)

func TestLocationFromIPs(t *testing.T) {
	tests := []struct {
		name       string
		input      []string
		want       string
		expectfail bool
	}{
		{"Single IP", []string{"1.2.3.4"}, "1.2.3.4", false},
		{"Both wfo/wfh IP", []string{"192.168.1.1", "192.168.1.2"}, "192.168.1.1", false},
		{"No IP", []string{""}, "", true},
	}

	// Run the tests
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ans, err := checkIPs(tt.input)
			if tt.expectfail {
			    fmt.Println("expectfail :)")
			    fmt.Printf("err: %s", err)
				if err == nil {
					t.Errorf("got %s, want %s", ans, tt.want)
				}
			} else {
			    fmt.Println("Not expectfail :)")
				if err != nil {
					t.Errorf("error %s", err)
				}
				if ans != tt.want {
					t.Errorf("got %s, want %s", ans, tt.want)
				}
			}
		})
	}
}

func TestGetIPs(t *testing.T) {
	ips, err := getIPs()
	hasLoopback := false
	if err != nil {
		t.Errorf("error %s", err)
	}

	for _, ip := range ips {
		if strings.HasPrefix(ip, "127.0.0.1") {
			hasLoopback = true
			break
		}
	}

	if !hasLoopback {
		t.Errorf("error %s", err)
	}
}

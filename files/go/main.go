package main

import (
	"errors"
	"fmt"
	"net"
	"os"

	"time"

	"flag"
)

func getIPs() ([]string, error) {
	var ips []string
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ips, err
	}
	for _, addr := range addrs {
		if ipNet, ok := addr.(*net.IPNet); ok {
			if ipNet.IP.To4() != nil {
				ips = append(ips, ipNet.IP.String())
			}
		}
	}
	return ips, nil
}

func checkIPs(ips []string) (string, error) {
	fmt.Printf("len ips %d", len(ips))
	if len(ips) == 0 {
		return "", errors.New("Empty ips input!")
	} else if ips[0] == "" {
		return "", errors.New("Empty first IPs input!")
	}
	return ips[0], nil
}

func getMidnightTS() int64 {
	// Get timestamp for eod https://pkg.go.dev/time#Unix
	currentTime := time.Now()
	nextMidnight := time.Date(currentTime.Year(), currentTime.Month(),
		currentTime.Day()+1, 0, 0, 0, 0, currentTime.Location())
	unixTimestamp := nextMidnight.Unix()

	return unixTimestamp
}

func main() {
	_, ok := os.LookupEnv("MY_TOKEN")
	if !ok {
		fmt.Println("Missing MY_TOKEN in environment")
		os.Exit(1)
	}

	str := flag.String("str", "default-value", "Nice str")
	dryrun := flag.Bool("dryrun", false, "Don't set anything in slack: 'true' 'false'")
	flag.Parse()
	fmt.Printf("Got  setting: '%s'\n", *str)
	fmt.Printf("Got dryrun setting: '%t'\n", *dryrun)

	ips, err := getIPs()
	if err != nil {
		fmt.Printf("Dang error: %s", err)
		os.Exit(1)
	}
	fmt.Printf("IPs: %s", ips)
	ip, err := checkIPs(ips)
	if err != nil {
		fmt.Printf("Dang error: %s", err)
		os.Exit(1)
	}
	fmt.Printf("Nice IP: %s", ip)

	if !*dryrun {
		fmt.Printf("This is not dryrun, run sensitive stuff here man\n")
	}
}

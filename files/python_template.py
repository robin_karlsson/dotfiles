#!/usr/bin/env python3
"""
This is an example docstring
"""

import argparse


def parse_args():
    """
    Parse the args
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", "--number", type=int, default=1337, help="Just a number"
    )
    parser.add_argument("-l", "--list-of-strings", nargs="+", default=[])

    #  parser.add_argument(
    #      "days", type=int, default=1, help="Just some days"
    #  )
    args = parser.parse_args()

    return args


def main():
    """
    Main function
    """
    args = parse_args()

    print("Parsed args successfully")
    print(f"This is your favorite number: {args.number}")


# Avoid running the code directly in case we import it elsewhere.
if __name__ == "__main__":
    main()

#!/usr/bin/env bash

set -euo pipefail

# Pick one. cd to script path or root?
# cd "$(realpath "${0%/*}")"
# cd "$(git -C "$(realpath "${0%/*}")" rev-parse --show-toplevel)"

################################################################################

function usage {
    cat <<EOF
Usage: $(basename "${0}") [--help]

Options:
  -h, --help           Show this help text.

EOF
}

function parse-args {
    local script opts

    script="$(basename "${0}")"

    opts="$(getopt --options hu:s: \
        --long help,user:,secret: \
        --name "${script}" \
        -- "$@")"
    eval set -- "$opts"

    USER=""
    SECRET=""

    while true; do
        case "$1" in
        -h | --help)
            usage
            exit
            ;;
        -u | --user)
            USER=""
            shift 1
            ;;
        -s | --secret)
            SECRET="${2}"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *) break ;;
        esac
    done

    # Verify options and arguments.
    if [ "${#USER}" -lt 2 ]; then
        echo "ERROR: The USER needs to be at least 2 chars!"
        echo
        usage
        exit 2
    fi >&2

    # Export all arguments and options here at the end of the function.
    export SECRET
    export USER
}

################################################################################

function funcy {
    local param
    param="${1:-default}"

    echo "$param"
    return 0
}

function main {
    parse-args "${@}"
    funcy 1337
}

main "${@}"

# The Robin's dotfiles.
The Robin dotfiles are intended for setting up a local development environment,
mostly focused on terminal utilities such as tmux, vim, zsh, etc.

## Installation:
- Make sure you have ansible installed.
- Run `make setup` in your favorite shell (zsh).
